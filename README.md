# Zelków

Zelków is a multi-language project, which, for now, hasn't a precise goal.
It deals with data, languages, and bored students.

## Authors

_If your name is missing, please add it._

* **Uinelj**, rust
* **Monsieur**, ror (Ruby on Rails)

## Contributing

To contribute, just create a folder named by your language in the `src` folder.
Then do whatever the fuck you want to, but keep in mind that your software needs to be easily controlled by another software (API, non-interactive console calls, etc.), in order to be used in the larger project.

If anyone has some problems about some rules here, feel free to create an issue.
